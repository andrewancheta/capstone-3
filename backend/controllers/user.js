const User = require('../models/user')
const bcrypt = require('bcrypt')
const auth = require('../auth')

//function to catch & handle errors
const errCatcher = err => console.log(err)

//function for finding duplicate emails
module.exports.emailExists = (params) => {
    //find a user document with matching email
    return User.find({ email: params.email })
    .then(result => {
        //if match found, return true
		return result.length > 0 ? true : false
    })
    .catch(errCatcher)
}

module.exports.register = (params) => {
    //instantiate a new user object
	let user = new User({
		email: params.email,
		password: bcrypt.hashSync(params.password, 10)
	})

    //save user object as a new document
    return user.save()
    .then((user, err) => {
        //if err generated, return false otherwise return true
		return (err) ? false : true
    })
    .catch(errCatcher)
}

module.exports.login = (params) => {
    //find a user with matching email
    return User.findOne({ email: params.email })
    .then(user => {
        //if no match found, return false
		if (user === null) return false

        //check if submitted password matches password on record
		const isPasswordMatched = bcrypt.compareSync(params.password, user.password)

        //if matching password
		if (isPasswordMatched) {
            //generate JWT
			return { accessToken: auth.createAccessToken(user.toObject()) }
		} else {
			return false
		}
    })
    .catch(errCatcher)
}

//function for getting details of a user based on a decoded token
module.exports.getPrivate = (params) => {
    return User.findById(params.userId)
    .then(user => {
        //clear the password property of the user object for security
		user.password = undefined
		return user
    })
    .catch(errCatcher)
}

// module.exports.addTravel = (params) => {
//     return User.findById(params.userId)
//     .then((user, err) => {
//         if(err) return false
//         user.travels.push(params.travel)
//         return user.save()
//         .then((updatedUser, err) => {
//             return err ? false : true
//         })
//         .catch(errCatcher)
//     })
//     .catch(errCatcher)
// }

module.exports.addCategory = (params) => {
    return User.findById(params.userId).then(user => {
        user.categories.push({ name: params.name, type: params.typeName })

        return user.save().then((user, err) => {
            return (err) ? false : true
        })
    })
} 

module.exports.addRecord = (params) => {
    return User.findById(params.userId).then(user => {
        let balanceAfterTransaction = 0

        if (user.transactions.length !== 0) {
            const balanceBeforeTransaction = user.transactions[user.transactions.length - 1].balanceAfterTransaction
            
            if (params.typeName === 'Income') {
                balanceAfterTransaction = parseInt(balanceBeforeTransaction) + parseInt(params.amount)
            }
            else {
                balanceAfterTransaction = parseInt(balanceBeforeTransaction) - parseInt(params.amount)
            }
        } else {
            balanceAfterTransaction = parseInt(params.amount)
        }

        user.transactions.push({
            categoryName: params.categoryName,
            type: params.typeName,
            amount: params.amount,
            description: params.description,
            balanceAfterTransaction: balanceAfterTransaction
        })

        return user.save().then((user, err) => {
            return (err) ? false : true
        })
    })
}


module.exports.addRecord = (params) => {
    return User.findById(params.userId).then(user => {
        let balanceAfterTransaction = 0

        if (user.transactions.length !== 0) {
            const balanceBeforeTransaction = user.transactions[user.transactions.length - 1].balanceAfterTransaction
            
            if (params.typeName === 'Income') {
                balanceAfterTransaction = parseInt(balanceBeforeTransaction) + parseInt(params.amount)
            }
            else {
                balanceAfterTransaction = parseInt(balanceBeforeTransaction) - parseInt(params.amount)
            }
        } else {
            balanceAfterTransaction = parseInt(params.amount)
        }

        user.transactions.push({
            categoryName: params.categoryName,
            type: params.typeName,
            amount: params.amount,
            description: params.description,
            balanceAfterTransaction: balanceAfterTransaction
        })

        return user.save().then((user, err) => {
            return (err) ? false : true
        })
    })
}

module.exports.get = (params) => {

    return User.findById(params.userId).then(user => {
        return { email: user.email, name: user.name, records: user.transactions, categories:user.categories, userId: user._id }
    })
}

module.exports.getCategories = (params) => {
    return User.findById(params.userId).then(user => {
        if (typeof params.typeName === 'undefined') {
            return user.categories
        }

        return user.categories.filter((category) => {
            if (category.type === params.typeName) {
                return category
            }
        })
    })
}

module.exports.getMostRecentRecords = (params) => {
    return User.findById(params.userId).then(user => {
        return  user.transactions.reverse() // .slice(0, 5) to retrieve first 5 records.
    })
}

module.exports.getRecordsByRange = (params) => {
    return User.findById(params.userId).then(user => {
        const recentRecords = user.transactions.filter((transaction) => {
            const isSameOrAfter = moment(transaction.dateAdded).isSameOrAfter(params.fromDate, 'day')
            const isSameOrBefore = moment(transaction.dateAdded).isSameOrBefore(params.toDate, 'day')

            if (isSameOrAfter && isSameOrBefore) {
                return transaction
            }
        })
        return recentRecords
    })
}

module.exports.getRecordsBreakdownByRange = (params) => {
    return User.findById(params.userId).then(user => {
        const summary = user.categories.map((category) => {
            return { categoryName: category.name, totalAmount: 0 }
        })

        user.transactions.filter((transaction) => {
            const isSameOrAfter = moment(transaction.dateAdded).isSameOrAfter(params.fromDate, 'day')
            const isSameOrBefore = moment(transaction.dateAdded).isSameOrBefore(params.toDate, 'day')

            if (isSameOrAfter && isSameOrBefore) {
                for (let i = 0; i < summary.length; i++) {
                    if (summary[i].categoryName === transaction.categoryName) {
                        summary[i].totalAmount += transaction.amount
                    }
                }
            }
        })

        return summary
    })
}

module.exports.searchRecord = (params) => {
    let records = []

    return User.findById(params.userId).then(user => {
        console.log(params)
        if (params.searchType === 'All') {
            records = user.transactions.filter((transaction) => {
                const hasIncludedKeyword = transaction.description.toLowerCase().includes(params.searchKeyword.toLowerCase())

                if (hasIncludedKeyword) {
                    return transaction
                }
            })
        } else {
            records = user.transactions.filter((transaction) => {
                const hasIncludedKeyword = transaction.description.toLowerCase().includes(params.searchKeyword.toLowerCase())
                const isSelectedType = transaction.type === params.searchType

                if (hasIncludedKeyword && isSelectedType) {
                    return transaction
                }
            })
        }

        return records.reverse()
    })
}