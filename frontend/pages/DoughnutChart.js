import { Doughnut } from 'react-chartjs-2';

export default function DoughnutChart({ categoryName, type, amount}){
	return (
		<Doughnut data={{
			datasets: [{
				data: [categoryName, type, amount],
				backgroundColor: ["blue", "orange", "red"]
			}],
			labels: ["categoryName", "type", "amount"]
		}} redraw={false}/>
	)
}